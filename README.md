
Chat app' - build with Express & SocketIO
================

[![SensioLabsInsight](https://insight.sensiolabs.com/projects/b5970128-f2c5-48b8-9a03-e26a2a7992a0/small.png)](https://insight.sensiolabs.com/projects/b5970128-f2c5-48b8-9a03-e26a2a7992a0)
[![Build Status](https://travis-ci.org/chalasr/Express_Socketio_Chat.svg?branch=master)](https://travis-ci.org/chalasr/Express_Socketio_Chat)

This application was created by Robin CHALAS - FullStack Web Developer -  http://www.chalasdev.fr/

Problems? Issues?
--------------

Write a message on chalasdev.fr or create an issue

This application requires:
-------------

- NodeJS
- Npm

Getting Started
---------------

- Clone this repository

``` git clone https://github.com/chalasr/Express_Socketio_Chat.git ```

- Install packages using NPM

``` npm install ```

- Start server

``` node server.js```

Enjoy !

Credits
-------

Author : [Robin Chalas](http://www.chalasdev.fr/)

License
-------

Copyright (c) 2014-2015 [Robin Chalas](http://www.chaladev.fr/) [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html)
